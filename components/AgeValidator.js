import React, {Component} from 'react';
import {View,Text,TextInput,TouchableOpacity,StyleSheet} from 'react-native';

const AgeValidator = props =>(

    <View>
        <Text>Ingrese su edad:</Text>
        <TextInput
            style={{height:40, borderColor:'gray',borderWidth:1}}
            onChangeText={props.onTextChange}
            value={props.value}
        />

    </View>
)




export default AgeValidator;