
import React, {Component} from 'react';
import {View,Text} from 'react-native';
import { SafeAreaView, FlatList, StyleSheet, Image} from 'react-native';

const DATA = [
    {
        id:'1',
        date:'01 de enero',
        name:'name1',
        url:'https://todosignificados.com/wp-content/uploads/2019/06/0c6a3ba5a4d68f0c09a1498ef85b16e5-1.jpg',
    },
    {
        id:'2',
        date:'02 de febrero',
        name:'name2',
        url:'https://i.ytimg.com/vi/jNruZfL4h6Y/maxresdefault.jpg',
    },
    {
        id:'3',
        date:'03 de febrero',
        name:'name3',
        url:'https://i7.pngguru.com/preview/681/169/285/kavaii-anime-drawing-art-anime-thumbnail.jpg',
    },
    {
        id:'4',
        date:'04 de marzo',
        name:'name4',
        url:'https://i.ytimg.com/vi/uglqx_0ms4E/hqdefault.jpg',
    },



]

function Item({date,name,url}) {
    return (
        <View style={styles.item}> 
            <Image
                style={styles.tinyLogo}
                source={{ uri: url }} 
            />
            <View style={styles.itemText}>
                <Text style={styles.title}>{date}</Text>
                <Text>{name}</Text>
            </View>
            
        </View>
    );
  }

const MyList = props =>(

  
    <SafeAreaView style={styles.container}>
        <Text>Mi lista:</Text>
        <FlatList
            data={DATA}
            renderItem={({ item }) => <Item id={item.id} date={item.date} name={item.name} url={item.url} />}
            keyExtractor={item => item.id}
        />
        
       
    </SafeAreaView>
    
      
   
)

const styles = StyleSheet.create({
    container: {
      flex: 1,
      
    },
    item: {
      backgroundColor: 'skyblue',
      padding: 20,
      marginVertical: 8,
      marginHorizontal: 16,
      flexDirection: 'row'
    },
    itemText:{
        marginVertical: 8,
        marginHorizontal: 16,
    },
    title: {
      fontSize: 32,
    },
    tinyLogo: {
        width: 150,
        height: 150,
      },
    
  });

export default MyList;